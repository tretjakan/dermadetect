const dialogflow = require('dialogflow');
const uuid = require('uuid');
require('dotenv').config();

const { DialoflowActions } = require('../utils/enums');

const sessionId = uuid.v4();

async function onHandleIntent(projectId, text) {
    // Create a new session
    const sessionClient = new dialogflow.SessionsClient({ keyFilename: process.env.GOOGLE_CRED });
    const sessionPath = sessionClient.sessionPath(projectId, sessionId);
   
    // The text query request.
    let request = {
      session: sessionPath,
      queryInput: {
        text: {
          text,
          languageCode: 'en-US',
        },
      },
    };
   
    // Send request and log result
    let responses = await sessionClient.detectIntent(request);
    console.log('Detected intent');
    let result = responses[0].queryResult;
    console.log(`  Query: ${result.queryText}`);
    if (result.action === DialoflowActions.imageHandler && text === 'yes') {
      result = { intent: {} };
      result.fulfillmentText = 'Waiting for image';
      result.fulfillmentMessages = [ { message: '' } ];
    }
    console.log(`  Response: ${result.fulfillmentText}`);
    if (result.intent) {
      console.log(`  Intent: ${result.intent.displayName}`);
    } else {
      console.log(`  No intent matched.`);
    }
    return result;
  };
  
  module.exports = {
    onHandleIntent,
  };