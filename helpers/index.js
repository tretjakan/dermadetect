const { botReply } = require('./bot.reply');
const { sendWebhookData, getImageUrl } = require('./webhooks');

module.exports = {
  botReply,
  sendWebhookData,
  getImageUrl,
};
