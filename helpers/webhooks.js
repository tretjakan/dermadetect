const axios = require('axios');

const sendWebhookData = async (webhook, data) => {

  try {
    const response = await axios.post(
      webhook,
      data,
      { headers: { 'Content-Type': 'application/json' } }
    );
    console.log(response.data);
    return response;
  } catch (error) {
    console.error(error);
    return error;
  }
}

const getImageUrl = async (token, fileId) => {
  try {
    const response = await axios.get(
      `https://api.telegram.org/bot${token}/getFile?file_id=${fileId}`,
      { headers: { 'Content-Type': 'application/json' } }
    );
    console.log(response.data);
    return response.data;
  } catch (error) {
    console.error(error);
    throw new Error(error);
  }
}

module.exports = {
  sendWebhookData,
  getImageUrl,
}