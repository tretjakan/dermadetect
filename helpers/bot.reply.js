const { to } = require('await-to-js');

const { onHandleIntent } = require('./dialogflow');
const { quickReplyHandler } = require('./callback-buttons');
const { DialogflowTypes, ErrorTexts, DialoflowActions } = require('../utils/enums');
const { sendWebhookData } = require('./webhooks');
require('dotenv').config();

const botReply = async (msg, reply, msgData) => {
  let intent = null;

  /**
   * Find Dialogflow intent related to value 
   */
  try {
    intent = await onHandleIntent(process.env.DIALOGFLOW_APP_NAME, msg);
  } catch (err) {
    console.error(err);
    return reply(ErrorTexts[404]);
  }

  if (!intent || !intent.fulfillmentMessages) {
    return reply(ErrorTexts[404]);
  }

  /**
   * GENERAL: Find right condition for send correct template to Telegram
   */

  try {
    const { fulfillmentMessages, fulfillmentText } = intent;

    if (intent.action === DialoflowActions.email) {
      /**
       * Send history to client webhook.
       */
      intent.id = msgData.id; // set user id as telegram id
      sendWebhookData(process.env.HISTORY_WEBHOOK, intent);
    }

    if (intent.action === DialoflowActions.freetext) {
      /**
       * Send message for check freetext from user
       */
      intent.id = msgData.id; // set user id as telegram id
      sendWebhookData(process.env.NLP_WEBHOOK, intent);
    }

    /**
     * We have multiple messages
     */

    if (fulfillmentMessages.length > 1 && fulfillmentMessages[0].message !== DialogflowTypes.quickReplies) {

      /**
       * Condition if we have a custom payload with keyboard markup
       */
  
      if (fulfillmentMessages[0].platform === process.env.PLATFORM && fulfillmentMessages[0].message === 'payload') {
        const { payload: { fields: { telegram: { structValue: { fields } } } } } = fulfillmentMessages[0];
  
        let body = [];
        fields.reply_markup.structValue.fields.keyboard.listValue.values.forEach((value) => body.push({ ...value.listValue.values }));
  
        const keyboardTemplate = [];
  
        body.forEach((value) => {
          const oneColumnKeyboard = [];
          Object.values(value).forEach(item => {
            oneColumnKeyboard.push(item.stringValue);
          });
          keyboardTemplate.push(oneColumnKeyboard);
        })
  
        return reply(fields.text.stringValue,
          {
            reply_markup: {
              keyboard: keyboardTemplate,
              resize_keyboard: true,
              one_time_keyboard: true,
            },
          });
      }

      /**
       * If we have just text answers from DF.
       */
      // if (intent.action === DialoflowActions.welcome) {
      //   // TODO: strange situation, need to reverse array for right text position. needed to fix in future.
      //   fulfillmentMessages.reverse();
      // }
      for (let i = 0; i < fulfillmentMessages.length; i += 1) {
        const [err, res] = await to(reply(fulfillmentMessages[i].text.text[0]));
        if (err) {
          console.error(err);
          return reply(ErrorTexts[404]);
        };
      }
      return;
    }

    /**
     * Handle quick replies from DF.
     */
    if (fulfillmentMessages[0].message === DialogflowTypes.quickReplies) {
      const inlineKeyboardTemplate = quickReplyHandler(intent, reply);
  
      return reply(inlineKeyboardTemplate.title, inlineKeyboardTemplate.body);
    }

    /**
     * Send only one text message.
     */
    return reply(fulfillmentText);
  } catch (error) {
    console.error(error);
    return reply(ErrorTexts[404]);
  }
}

module.exports = {
  botReply,
};
