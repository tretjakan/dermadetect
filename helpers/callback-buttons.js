const quickReplyHandler = (dfBody) => {

  const body = dfBody.fulfillmentMessages[0].quickReplies.quickReplies;

  return {
    title: dfBody.fulfillmentMessages[0].quickReplies.title,
    body: {
      reply_markup: {
        keyboard: [body],
        resize_keyboard: true,
        one_time_keyboard: true,
      },
    },
  }
};

module.exports = {
  quickReplyHandler,
};
