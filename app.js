    
const Telegraf = require('telegraf');
const express = require('express');
const { botReply, sendWebhookData, getImageUrl } = require('./helpers');
const { TelegramHandlers, ErrorTexts } = require('./utils/enums');
require('dotenv').config();

const TOKEN = process.env.TELEGRAM_TOKEN;
const ImageHandlerText = 'chatbotsstudio';
const WEBHOOK = process.env.WEBHOOK;

const bot = new Telegraf(TOKEN);

/**
 * Set telegram webhook
 */

bot.telegram.setWebhook(`${WEBHOOK}/${TOKEN || 'secret-path'}`);

/**
 * Photo handler
 */
bot.on(TelegramHandlers.photo, async ({ reply, message }) => {
  let imagePath = null;
  const { from: { id } } = message;
  /**
   * Get image path
   */
  try {
    const [file] = message.photo.sort((a, b) => b.file_size - a.file_size);
    imagePath = await getImageUrl(TOKEN, file.file_id);
  } catch (error) {
    console.error(error);
    return reply(ErrorTexts.imageError);
  }
  if (!imagePath || !imagePath.ok) {
    return reply(ErrorTexts.imageError);
  }
  const { result } = imagePath;
  /**
   * Send image data to webhook
   */
  sendWebhookData(process.env.IMAGES_WEBHOOK, { id, image: `https://api.telegram.org/file/bot${TOKEN}/${result.file_path}` });
  return botReply(ImageHandlerText, reply);
});

/**
 * Callback buttons handler (inline_keyboard markup)
 */
bot.on(TelegramHandlers.callbackData, async ({ reply, update }) => {

  return botReply(update.callback_query.data, reply);
})

/**
 * Just text handler
 */
bot.on(TelegramHandlers.text, async ({ message, reply }) => {

  return botReply(message.text, reply, message.from);
});

/**
 * APP CONFIGURATION
 */
const app = express();
app.get('/', (req, res) => res.send('Hello World!'));

app.use(bot.webhookCallback('/' + TOKEN || 'secret-path'));

app.listen(process.env.PORT || 8443, () => {
  console.log(`Example app listening ${process.env.PORT || 8443}!`)
});
