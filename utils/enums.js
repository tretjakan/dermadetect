const DialogflowTypes = {
    quickReplies: 'quickReplies',
};

const TelegramHandlers = {
    photo: 'photo',
    text: 'text',
    callbackData: 'callback_query',
};

const ErrorTexts = {
    404: 'Sorry, can\'t understand you or something went wrong, try please again.',
    imageError: 'Sorry, can\'t get image now. Try it, please, later.',
};

const DialoflowActions = {
    email: 'emailHandler',
    welcome: 'input.welcome',
    imageHandler: 'image_handler',
    freetext: 'freetext',
};

module.exports = {
    DialogflowTypes,
    TelegramHandlers,
    ErrorTexts,
    DialoflowActions,
};

// 754590992:AAFBgLqp9_3puo2kD2wF8v3fRbthQkVhZG0